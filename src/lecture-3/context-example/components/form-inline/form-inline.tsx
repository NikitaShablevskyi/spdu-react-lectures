import React, {FC, ReactNode} from 'react';

type Props = {
    children: ReactNode
};

export const FormInline: FC<Props> = ({children}) => {
    return (<form className="form-inline">
            {children}
        </form>
    )
};
