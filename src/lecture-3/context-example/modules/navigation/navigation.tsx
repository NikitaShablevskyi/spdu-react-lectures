import React, {FC} from 'react';
import {Navbar, Item as NavbarItem, Nav, NavbarLink} from '../../components/navbar';
import {FormInline} from '../../components/form-inline';
import {UserInfo} from '../user-info/user-info';

type Props = {};

export const Navigation: FC<Props> = () => {
    return (
        <Navbar>
            <Nav>
                <NavbarItem>
                    <NavbarLink url="#" caption="Item 1"/>
                </NavbarItem>
                <NavbarItem>
                    <NavbarLink url="#" caption="Item 2"/>
                </NavbarItem>
                <NavbarItem>
                    <NavbarLink url="#" caption="Item 3"/>
                </NavbarItem>
                <NavbarItem>
                    <NavbarLink url="#" caption="Item 4"/>
                </NavbarItem>
            </Nav>
            <FormInline>
                <UserInfo/>
            </FormInline>
        </Navbar>
    )
};
